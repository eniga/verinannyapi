﻿using System;
namespace VerinannyApi.Models
{
    public class Response
    {
        public bool Status { get; set; }
        public string Description { get; set; }
    }
}
