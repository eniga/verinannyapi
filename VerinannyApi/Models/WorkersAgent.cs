﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VerinannyApi.Models
{
    [Table("domestic_workers_agent")]
    public class WorkersAgent
    {
        [Key]
        public int id { get; set; }
        public DateTime archived { get; set; }
        public DateTime date_created { get; set; }
        public string full_name { get; set; }
        public string email { get; set; }
        public string mobile_phone1 { get; set; }
        public string mobile_phone2 { get; set; }
        public string gender { get; set; }
        public string home_address_street { get; set; }
        public string home_address_town { get; set; }
        public string home_address_state { get; set; }
        public string country { get; set; }
    }
}
