﻿using System;
namespace VerinannyApi.Models
{
    public class LoginResponse : Response
    {
        public Users user { get; set; }
    }

    public class LoginRequest
    {
        public string Username { get; set; }
    }
}
