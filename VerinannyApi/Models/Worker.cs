﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VerinannyApi.Models
{
    [Table("domestic_workers_worker")]
    public class Worker
    {
        [Key]
        public int id { get; set; }
        public DateTime archived { get; set; }
        public DateTime date_created { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string other_name { get; set; }
        [Editable(false)]
        public string full_name { get => string.Format("{0} {1}",first_name, last_name); }
        public string email { get; set; }
        public string mobile_phone1 { get; set; }
        public string mobile_phone2 { get; set; }
        public string gender { get; set; }
        public string service { get; set; }
        public DateTime birth_date { get; set; }
        public bool verified { get; set; }
        public DateTime last_modified { get; set; }
        public int current_employer_id { get; set; }
        public string home_town { get; set; }
        public string home_state { get; set; }
    }
}
