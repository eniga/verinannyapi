﻿using System;
using Dapper;

namespace VerinannyApi.Models
{
    [Table("users_user")]
    public class Users
    {
        [Key]
        public int id { get; set; }
        public string password { get; set; }
        public DateTime? last_login { get; set; }
        public bool is_superuser { get; set; }
        public string username { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        [Editable(false)]
        public string full_name { get => string.Format("{0} {1}", first_name, last_name); }
        public string email { get; set; }
        public bool is_staff { get; set; }
        public bool is_active { get; set; }
        public DateTime date_joined { get; set; }
        public string workplace { get; set; }
        public string job_title { get; set; }
        public string home_address_street { get; set; }
        public string home_address_lga { get; set; }
        public string home_address_town { get; set; }
        public string home_address_state { get; set; }
        public string home_address_country { get; set; }
        public string home_address_country_code { get; set; }
        public string home_address_postal_code { get; set; }
        public string mobile_phone1 { get; set; }
        public string mobile_phone2 { get; set; }
        public string gender { get; set; }
        public DateTime? birth_date { get; set; }
        public bool private_mode { get; set; }
        public bool verified { get; set; }
        public DateTime? verified_date { get; set; }
        public DateTime last_modified { get; set; }
        public int? verified_by_id { get; set; }
        public bool agreed_terms { get; set; }
    }
}
