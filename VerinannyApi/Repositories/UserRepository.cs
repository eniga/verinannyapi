﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using VerinannyApi.Models;

namespace VerinannyApi.Repositories
{
    public class UserRepository
    {
        private static string ConnectionString;

        public UserRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        private NpgsqlConnection GetConnection()
        {
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.PostgreSQL);
            return new NpgsqlConnection(ConnectionString);
        }

        public IEnumerable<Users> List()
        {
            List<Users> result = new List<Users>();
            try
            {
                using(IDbConnection db = GetConnection())
                {
                    result = db.GetList<Users>().AsList();
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }

        public Users GetById(int id)
        {
            Users result = new Users();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    result = db.Get<Users>(id);
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }

        public Users GetByUsername(string username)
        {
            Users result = new Users();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    result = db.GetList<Users>("Where username = @username", new { username }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }

        public Response Add(Users user)
        {
            Response response = new Response();
            try
            {
                using(IDbConnection db = GetConnection())
                {
                    //var list = db.GetList<Users>();
                    //var max = list.Max(x => x.id);
                    //int newid = max + 1;
                    //user.id = newid;
                    var exist = GetByUsername(user.username);
                    if(exist != null)
                    {
                        response.Status = true;
                        response.Description = "User exists";
                    }
                    db.Insert(user);
                    response.Status = true;
                    response.Description = "Successful";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Description = ex.Message;
            }
            return response;
        }

        public Response Update(Users user)
        {
            Response response = new Response();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    var exist = GetByUsername(user.username);
                    user.password = exist.password;
                    user.last_modified = DateTime.Now;
                    db.Update(user);
                    response.Status = true;
                    response.Description = "Successful";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Description = ex.Message;
            }
            return response;
        }

        public LoginResponse Authentication (LoginRequest req)
        {
            LoginResponse response = new LoginResponse();
            try
            {
                var details = GetByUsername(req.Username);
                if(details == null)
                {
                    response.Status = false;
                    response.Description = "User is not registered";
                    return response;
                }
                if (!details.is_active)
                {
                    response.Status = false;
                    response.Description = "User profile is not active";
                    return response;
                }
                details.last_login = DateTime.Now;
                Update(details);
                response.Status = true;
                response.Description = "Successful";
                response.user = details;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Description = ex.Message;
            }
            return response;
        }
    }
}
