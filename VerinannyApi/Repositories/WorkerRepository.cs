﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using VerinannyApi.Models;

namespace VerinannyApi.Repositories
{
    public class WorkerRepository
    {
        static string ConnectionString;

        public WorkerRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        private NpgsqlConnection GetConnection()
        {
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.PostgreSQL);
            return new NpgsqlConnection(ConnectionString);
        }

        public IEnumerable<Worker> List()
        {
            List<Worker> result = new List<Worker>();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    result = db.GetList<Worker>().AsList();
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }

        public Worker GetById(int id)
        {
            Worker result = new Worker();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    result = db.Get<Worker>(id);
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }

        public Worker GetByUsername(string username)
        {
            Worker result = new Worker();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    result = db.GetList<Worker>("Where username = @username", new { username }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }

        public Response Add(Worker user)
        {
            Response response = new Response();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    db.Insert(user);
                    response.Status = true;
                    response.Description = "Successful";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Description = ex.Message;
            }
            return response;
        }

        public Response Update(Worker user)
        {
            Response response = new Response();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    db.Update(user);
                    response.Status = true;
                    response.Description = "Successful";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Description = ex.Message;
            }
            return response;
        }

        public Response Delete(int id)
        {
            Response response = new Response();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    db.Delete<Worker>(id);
                    response.Status = true;
                    response.Description = "Successful";
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Description = ex.Message;
            }
            return response;
        }
    }
}
