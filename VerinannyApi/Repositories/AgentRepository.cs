﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Configuration;
using Npgsql;
using VerinannyApi.Models;

namespace VerinannyApi.Repositories
{
    public class AgentRepository
    {
        static string ConnectionString;

        public AgentRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
        }

        private NpgsqlConnection GetConnection()
        {
            SimpleCRUD.SetDialect(SimpleCRUD.Dialect.PostgreSQL);
            return new NpgsqlConnection(ConnectionString);
        }

        public IEnumerable<WorkersAgent> List()
        {
            List<WorkersAgent> result = new List<WorkersAgent>();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    result = db.GetList<WorkersAgent>().AsList();
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }

        public WorkersAgent GetUserById(int id)
        {
            WorkersAgent result = new WorkersAgent();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    result = db.Get<WorkersAgent>(id);
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }

        public WorkersAgent GetByUsername(string username)
        {
            WorkersAgent result = new WorkersAgent();
            try
            {
                using (IDbConnection db = GetConnection())
                {
                    result = db.GetList<WorkersAgent>("Where username = @username", new { username }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                result = null;
                Console.Write(ex);
            }
            return result;
        }
    }
}
