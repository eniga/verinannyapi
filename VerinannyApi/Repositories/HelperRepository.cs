﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace VerinannyApi.Repositories
{
    public static class HelperRepository
    {
        public static bool IsNumeric(string value)
        {
            bool ok = value.All(char.IsNumber);
            return ok;
        }

        public static string ObjectToXml(object obj)
        {
            var settings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = false,
                Encoding = Encoding.GetEncoding("UTF-16")
            };

            var namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            var serializer = new XmlSerializer(obj.GetType());

            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter, settings))
                {
                    serializer.Serialize(xmlWriter, obj, namespaces);
                }
                return stringWriter.ToString();
            }
        }

        public static string RemoveSpecialCharacters(string str)
        {
            return Regex.Replace(str, "[^a-zA-Z0-9.,]+/", "", RegexOptions.Compiled);
        }

        static Random random = new Random();

        public static string GenerateRandomNumber(int count)
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < count; i++)
            {
                int number = random.Next(10);
                builder.Append(number);
            }

            return builder.ToString();
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string GenerateSHA256String(string input)
        {
            var result = string.Empty;
            byte[] data = Encoding.UTF8.GetBytes(input);
            using (SHA256 shaM = new SHA256Managed())
            {
                var hash = shaM.ComputeHash(data);
                foreach (byte x in hash)
                {
                    result += String.Format("{0:x2}", x);
                }
            }
            return result;
        }

        public static string GenerateSHA512String(string input)
        {
            var result = string.Empty;
            byte[] data = Encoding.UTF8.GetBytes(input);
            using (SHA512 shaM = new SHA512Managed())
            {
                var hash = shaM.ComputeHash(data);
                foreach (byte x in hash)
                {
                    result += String.Format("{0:x2}", x);
                }
            }
            return result;
        }

        public static string GenerateSHA1String(string input)
        {
            var result = string.Empty;
            byte[] data = Encoding.UTF8.GetBytes(input);
            using (SHA1 shaM = new SHA1Managed())
            {
                var hash = shaM.ComputeHash(data);
                foreach (byte x in hash)
                {
                    result += String.Format("{0:x2}", x);
                }
            }
            return result;
        }

        public static string SHA1Hash(string input)
        {
            var hash = (new SHA1CryptoServiceProvider()).ComputeHash(Encoding.UTF8.GetBytes(input));
            string result = Convert.ToBase64String(hash);
            return result;
        }

        public static string SHA256Hash(string input)
        {
            var hash = (new SHA256CryptoServiceProvider()).ComputeHash(Encoding.UTF8.GetBytes(input));
            string result = Convert.ToBase64String(hash);
            return result;
        }

        public static string SHA512Hash(string input)
        {
            var hash = (new SHA512CryptoServiceProvider()).ComputeHash(Encoding.UTF8.GetBytes(input));
            string result = Convert.ToBase64String(hash);
            return result;
        }
    }
}
