﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VerinannyApi.Models;
using VerinannyApi.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VerinannyApi.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        UserRepository repo;

        public UserController(IConfiguration configuration)
        {
            repo = new UserRepository(configuration);
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Users> List()
        {
            var result = repo.List().ToList();
            result.ForEach(x => x.password = null);
            return result;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Users GetById(int id)
        {
            var result = repo.GetById(id);
            if (result != null)
                result.password = null;
            return result;
        }

        [HttpGet("User/{username}")]
        public Users GetByUsername(string username)
        {
            var result = repo.GetByUsername(username);
            if (result != null)
                result.password = null;
            return result;
        }

        // POST api/values
        [HttpPost]
        public Response Post([FromBody]Users value)
        {
            return repo.Add(value);
        }

        // PUT api/values/5
        [HttpPut]
        public Response Put([FromBody]Users value)
        {
            return repo.Update(value);
        }

        [HttpPost("Login")]
        public LoginResponse Authentication([FromBody] LoginRequest request)
        {
            var result = repo.Authentication(request);
            if(result.Status)
                result.user.password = null;
            return result;
        }
    }
}
