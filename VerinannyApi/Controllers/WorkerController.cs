﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VerinannyApi.Models;
using VerinannyApi.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VerinannyApi.Controllers
{
    [Route("api/[controller]")]
    public class WorkerController : Controller
    {
        WorkerRepository repo;

        public WorkerController(IConfiguration configuration)
        {
            repo = new WorkerRepository(configuration);
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<Worker> List()
        {
            return repo.List();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Worker Get(int id)
        {
            return repo.GetById(id);
        }

        // POST api/values
        [HttpPost]
        public Response Post([FromBody]Worker value)
        {
            return repo.Add(value);
        }

        // PUT api/values/5
        [HttpPut]
        public Response Put([FromBody]Worker value)
        {
            return repo.Update(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public Response Delete(int id)
        {
            return repo.Delete(id);
        }
    }
}
