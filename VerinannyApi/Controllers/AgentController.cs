﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VerinannyApi.Models;
using VerinannyApi.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VerinannyApi.Controllers
{
    [Route("api/worker/[controller]")]
    public class AgentController : Controller
    {
        AgentRepository repo;

        public AgentController(IConfiguration configuration)
        {
            repo = new AgentRepository(configuration);
        }

        // GET: api/values
        [HttpGet]
        public IEnumerable<WorkersAgent> List()
        {
            return repo.List();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public WorkersAgent Get(int id)
        {
            return repo.GetUserById(id);
        }
    }
}
